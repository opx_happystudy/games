#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int ref_table[3][3];

void print_title()
{
    printf("###### xenophage #####\n");
}

static void init_table()
{
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
           /// ref_table[i][j]=1+i*3+j; test
           ref_table[i][j]=1;


    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
            printf("%d ", ref_table[i][j]);
        printf("\n");
    }
}
void print_menu()
{
    print_title();
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
            printf("%d ", ref_table[i][j]%4);
        printf("\n");
    }
}

void run_table(int input) // 1,2,3,4,5,6,7,8,9
{
    printf("%d\n",input);
    switch (input) {
        case 1:
        ref_table[0][0]=ref_table[0][0]+1;
        ref_table[0][1]=ref_table[0][1]+1;
        ref_table[0][2]=ref_table[0][2]+1;
        ref_table[1][0]=ref_table[1][0]+1;
        ref_table[2][0]=ref_table[2][0]+1;

        break;
        case 2:
        ref_table[0][0]=ref_table[0][0]+1;
        ref_table[0][1]=ref_table[0][1]+1;
        ref_table[0][2]=ref_table[0][2]+1;
        ref_table[1][1]=ref_table[1][1]+1;
        ref_table[2][1]=ref_table[2][1]+1;
        break;
        case 3:
        ref_table[0][0]=ref_table[0][0]+1;
        ref_table[0][1]=ref_table[0][1]+1;
        ref_table[0][2]=ref_table[0][2]+1;
        ref_table[1][2]=ref_table[1][2]+1;
        ref_table[2][2]=ref_table[2][2]+1;
        break;
///////////////////////////////////////////////
        case 4:
        ref_table[1][0]=ref_table[1][0]+1;
        ref_table[1][1]=ref_table[1][1]+1;
        ref_table[1][2]=ref_table[1][2]+1;
        ref_table[0][0]=ref_table[0][0]+1;
        ref_table[2][0]=ref_table[2][0]+1;

        break;
        case 5:
        ref_table[1][0]=ref_table[1][0]+1;
        ref_table[1][1]=ref_table[1][1]+1;
        ref_table[1][2]=ref_table[1][2]+1;
        ref_table[0][1]=ref_table[0][1]+1;
        ref_table[2][1]=ref_table[2][1]+1;
        break;

        case 6:
        ref_table[1][0]=ref_table[1][0]+1;
        ref_table[1][1]=ref_table[1][1]+1;
        ref_table[1][2]=ref_table[1][2]+1;
        ref_table[0][2]=ref_table[0][2]+1;
        ref_table[2][2]=ref_table[2][2]+1;
        break;
///////////////////////////////////////////////
        case 7:
        ref_table[2][0]=ref_table[2][0]+1;
        ref_table[2][1]=ref_table[2][1]+1;
        ref_table[2][2]=ref_table[2][2]+1;
        ref_table[0][0]=ref_table[0][0]+1;
        ref_table[1][0]=ref_table[1][0]+1;

        break;
        case 8:
        ref_table[2][0]=ref_table[2][0]+1;
        ref_table[2][1]=ref_table[2][1]+1;
        ref_table[2][2]=ref_table[2][2]+1;
        ref_table[0][1]=ref_table[0][1]+1;
        ref_table[1][1]=ref_table[1][1]+1;

        break;

        case 9:
        ref_table[2][0]=ref_table[2][0]+1;
        ref_table[2][1]=ref_table[2][1]+1;
        ref_table[2][2]=ref_table[2][2]+1;
        ref_table[0][2]=ref_table[0][2]+1;
        ref_table[1][2]=ref_table[1][2]+1;
        break;

    }
    print_menu();
}

void input_main()
{
    char buf[256]={0,};
    int32_t i = 0;
    int32_t num = 0;

    while(1)
    {
        printf("#> ");
        fgets(buf, sizeof(buf), stdin);

        if( 'q' == buf[0] ) {
            printf("Exit.\n");
            break;
        } 
        else if( 'h' == buf[0] ) {
            print_menu();
            continue;
        }
        else if( 0x0A == buf[0] ) {
            continue;
        }
        else {
                
                num=atoi((const char *)buf);
                // printf("%d",num);
                run_table(num);
        }
    }
}
int main(int argc , char * argv[])
{
    init_table();
    if (argc == 2)
    {
        int num = atoi(argv[1])%4;
        print_title();
        // set inittype
        printf("init type %d\n",num);
        run_table(num);
    }
    input_main();
}

